const express = require("express");
const app = express();

const currencyRouter = require("./router/currecyRouter");

app.use(express.json());
app.use("/currency", currencyRouter);

module.exports = app;
