const dotEnv = require("dotenv");
const app = require("./app");
dotEnv.config({ path: "./config.env" });

app.listen(process.env.PORT || 4000, () => {
  console.log("Server is listening..." + process.env.PORT);
});
