const express = require("express");
const router = express.Router();
const currencyController = require("./../controllers/currencyController");

router
  .route("/formCurrency/:fromCurrency/toCurrency/:toCurrency")
  .get(currencyController.getLiveCurrency);

module.exports = router;
