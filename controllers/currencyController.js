const { json } = require("body-parser");
const request = require("request");

const getExchangeRate = (fromCurrency, toCurrency) => {
  return new Promise((resolve, reject) => {
    var url = `https://www.alphavantage.co/query?function=CURRENCY_EXCHANGE_RATE&from_currency=${fromCurrency}&to_currency=${toCurrency}&apikey=0KM9LBCMF12S7YLQ`;
    request.get(
      {
        url: url,
        json: true,
        headers: { "User-Agent": "request" },
      },
      (err, res, data) => {
        if (err) {
          console.log("Error:", err);
          reject(err);
        } else if (res.statusCode !== 200) {
          console.log("Status:", res.statusCode);
        } else {
          resolve(data);
        }
      }
    );
  });
};

exports.getLiveCurrency = async (req, res, next) => {
  const fromCurrency = req.params.fromCurrency;
  const toCurrency = req.params.toCurrency;

  try {
    const data = await getExchangeRate(fromCurrency, toCurrency);
    const all = data["Realtime Currency Exchange Rate"];
    const exchangeRate = Math.round(all["5. Exchange Rate"] * 1000) / 1000; //it will give us a exect round of value.
    const toCountry = all["4. To_Currency Name"];
    const fromCountry = all["2. From_Currency Name"];

    res.status(200).json({
      status: "success",
      toCountry,
      fromCountry,
      exchangeRate,
    });

    console.log(data);
  } catch (err) {
    res.status(res.statusCode || 400).json({
      status: "failure",
      message: "something went very wrong!",
    });
  }
};
